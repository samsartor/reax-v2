#![feature(arbitrary_self_types)]

use hornpipe_core::{Counter, Data, Own, Register, Transaction};

hornpipe_core::declare_universe!(OtherUniverse);

#[derive(Data)]
pub struct TestState {
    pub a: u32,
    pub b: Register<u32>,
    pub c: Counter<i32>,
}

fn main() {
    let state = Own::new(TestState {
        a: 0xdeadbeef,
        b: Register::new(0),
        c: Counter::new(0),
    });

    {
        let tx = Transaction::start();
        let ty = Transaction::start();
        let x = state.refer().attach(&tx).unwrap();
        dbg!(*x.a());
        x.b().write(42);
        dbg!(x.b().read());
        let mut c = x.c();
        c += 1;
        dbg!(x.c().read());

        let y = state.refer().attach(&ty).unwrap();
        dbg!(*y.a());
        dbg!(y.b().read());
        dbg!(y.c().read());

        tx.commit();
    }

    {
        let tz = Transaction::start();
        let z = state.refer().attach(&tz).unwrap();
        dbg!(*z.a());
        dbg!(z.b().read());
        dbg!(z.c().read());
    }
}
