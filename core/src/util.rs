// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

pub use crate::share::MaybeShared;
use crate::{self as hornpipe_core};
use hornpipe_macros::ProjectMaybeShared;
use std::mem::replace;

use crate::share::CopyRef;
