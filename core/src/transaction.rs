// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//! Responsible for the isolation between different transactions.

use crate::data::Opaque;
use crate::framing::{DataFraming, DataPtr, UnknownData};
use crate::refs::{PtrCmp, RefIdentity};
use crate::share::{OwnershipStatus, PartiallyOwned};
use crate::utex::{Unihandle, Unitex};
use crate::{self as hornpipe_core};
use crate::{Data, DefaultUniverse, Own, Ref, Universe};
use crossbeam_epoch::{pin, Guard};
use parking_lot::ReentrantMutex;
use std::alloc::{alloc, dealloc};
use std::cell::RefCell;
use std::collections::{BTreeSet, HashMap};
use std::ffi::c_void;
use std::mem::{replace, transmute};
use std::sync::atomic::{AtomicBool, AtomicU64, Ordering};

mod finalize;
mod sequencer;
pub use finalize::finalize;
pub use sequencer::*;

/// A transaction used to access consistant views of memory.
#[must_use]
pub struct Transaction<U: Universe = DefaultUniverse> {
    reference: TransactionRef<U>,
    // This field drives me sort of crazy. Because this pins crossbeam_epoch for the entire duration
    // of a transaction, it prevents any memory being freed anywhere in hornpipe as long as a single
    // transaction is still open. Gross! Not only is that terrible leak escelation, it makes transactions
    // less useful in async functions that keep a transaction open for any aribtrary time. Even worse:
    //
    // **this field is not at all required in theory**
    //
    // The whole point of transactions is that they are isolated from mutations which would invalidate
    // accessable pointers. So the guard should only be needed while deep inside the transaction logic
    // that decides whether or not each pointer is accessable. Alas, the transaction system is embeded
    // inside a larger context it can not fully control. If someone drops a StrongGrc, only the guard
    // is capible of keeping it alive, the finalizer/sequencer can not *currently* intervene:
    // ```rust
    //  let owned_thing = Thing::new(); // holds ownership of some memory
    //  let attached_thing = owned_thing::refer().attach(&tx); // does not borrow owned_thing
    //  let borrowed_data = attached_thing.contents(); // pointer to the contents of owned_thing
    //  drop(owned_thing); // defered by crossbeam_epoch, but not by the sequencer
    //  *borrowed_data; // OOPS!
    //  ```
    //  The only avoids UB because of this guard field. :(
    //
    // But it is also very hard to remove this field for logistical reasons. With the guard avalible,
    // functions like logic::read_object are trivially safe. But lengthening the lifetime beyond the
    // guard relies on the correctness of the entire hornpipe system, and especially on the correctness
    // of the finalize module. At time of writing, there is not enough testing in place to take that risk.
    guard: Guard,
    will_finish: bool,
}

impl Transaction {
    /// Start a new transaction.
    #[inline(always)]
    pub fn start() -> Self {
        Transaction {
            reference: start_ref(),
            guard: pin(),
            will_finish: true,
        }
    }
}

impl<U: Universe> Transaction<U> {
    /// Start a new transaction.
    #[inline(always)]
    pub fn start_in(_universe: U) -> Self {
        Transaction {
            reference: start_ref(),
            guard: pin(),
            will_finish: true,
        }
    }

    /// Commit the changes made by this transaction to memory. *If this is not called, all changes will be ignored.*
    #[inline(always)]
    pub fn commit(mut self) -> Submission<U> {
        let submission = Submission {
            reference: self.reference,
            will_finish: self.will_finish,
        };
        self.will_finish = false;
        submit_ref(self.reference);
        submission
    }

    /// Create a sub-transaction _which will not finish the given transaction when dropped_.
    #[inline(always)]
    pub fn resume(reference: TransactionRef<U>) -> Self {
        Transaction {
            reference,
            guard: pin(),
            will_finish: false,
        }
    }

    /// Provide the weak reference underlying this transaction.
    #[inline(always)]
    pub fn refer(&self) -> TransactionRef<U> {
        self.reference
    }

    /// Provide the guard which prevents memory from being freed while this transaction is unfinished.
    #[inline(always)]
    pub fn guard(&self) -> &Guard {
        &self.guard
    }
}

impl<U: Universe> Drop for Transaction<U> {
    #[inline(always)]
    fn drop(&mut self) {
        if self.will_finish {
            finish_ref(self.reference);
            finalize::<U>();
        }
    }
}

pub struct Submission<U: Universe = DefaultUniverse> {
    reference: TransactionRef<U>,
    will_finish: bool,
}

impl<U: Universe> Submission<U> {
    /// Provide the weak reference underlying the submitted transaction.
    #[inline(always)]
    pub fn refer(&self) -> TransactionRef<U> {
        self.reference
    }

    /// Create a transaction that attempts to undoe all the operations
    /// produced by the submitted transaction. Must be submitted in
    /// order to take effect.
    pub fn undo(&self) -> Transaction<U> {
        todo!()
    }
}

impl<U: Universe> Drop for Submission<U> {
    #[inline(always)]
    fn drop(&mut self) {
        if self.will_finish {
            finish_ref(self.reference);
            finalize::<U>();
        }
    }
}

#[derive(Debug)]
pub struct ReactionInfo<U: Universe> {
    target: RefIdentity,
    accessed: BTreeSet<RefIdentity>,
    sender: TransactionStepRef<U>,
}

#[derive(Data)]
pub struct TransactionData<U: Universe> {
    opaque: Opaque<OpaqueTransactionData<U>>,
}

pub struct OpaqueTransactionData<U: Universe> {
    viewpoint: Clock,
    visibility: AtomicU64,
    finished: AtomicBool,
    state: Unitex<ReentrantMutex<RefCell<TransactionState<U>>>, U>,
}

#[derive(Debug)]
struct DataCopy {
    ptr: *mut DataFraming<UnknownData>,
    modified: bool,
}

unsafe impl Send for DataCopy {}

impl DataCopy {
    fn copy_from(frame: &DataFraming<UnknownData>) -> Self {
        let layout = frame.meta.frame_layout();
        let bytes: *mut u8;

        assert!(layout.size() > 0);
        // SAFETY: checked for non-zero size
        bytes = unsafe { alloc(layout) };

        // SAFETY: we are copying into a valid pointer, from a valid pointer. The contents
        // are bitwise copyable per the rules on Data which all DataPtrs must contain impls of.
        unsafe {
            bytes.copy_from_nonoverlapping(frame as *const _ as *const u8, layout.size());
        }

        let ptr = bytes as *mut DataFraming<UnknownData>;

        // SAFETY: brand-new copy, and we are only going to access !Drop fields
        let copy = unsafe { &mut *ptr.as_mut().expect("alloc failed") };

        // There is exactly one user of the new copy (us)
        *copy.users.get_mut() = 1;

        // before producing PartiallyOwned, we need to ensure partial ownership is cleared
        unsafe {
            copy.meta.operations.set_partial_ownership(
                PartiallyOwned::new_mut(&mut copy.data),
                0b111,
                crate::share::OwnershipStatus::Copied,
            )
        };

        DataCopy {
            ptr,
            modified: false,
        }
    }

    fn as_ref(&self) -> &DataFraming<UnknownData> {
        unsafe { &*self.ptr }
    }

    fn make_mut(&mut self) -> &mut PartiallyOwned<DataFraming<UnknownData>> {
        self.modified = true;
        if self.as_ref().users.load(Ordering::Acquire) == 1 {
            // SAFETY: proven exclusive copy can't have gone anywhere new
            unsafe { &mut *self.ptr.cast() }
        } else {
            // Overwrite self so that it gets dropped if the count decreased since the last check.
            *self = DataCopy::copy_from(self.as_ref());
            // SAFETY: brand-new copy can't have gone anywhere
            unsafe { &mut *self.ptr.cast() }
        }
    }

    fn make_user(&self) -> Self {
        let prev = self.as_ref().users.fetch_add(1, Ordering::Relaxed);
        if prev == isize::MAX as usize {
            // We are screwed. This is what Arc does and I trust them.
            std::process::abort();
        }
        DataCopy {
            ptr: self.ptr,
            modified: false,
        }
    }
}

impl Drop for DataCopy {
    fn drop(&mut self) {
        if self.as_ref().users.fetch_sub(1, Ordering::AcqRel) == 1 {
            unsafe {
                dealloc(self.ptr as *mut u8, self.as_ref().meta.frame_layout());
            }
        }
    }
}

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq)]
pub enum CopyablePtr {
    Data(DataPtr),
    TreeNode(),
}

#[derive(Debug, Default)]
pub struct TransactionState<U: Universe> {
    next_step: u32,
    /// Copies
    data_copies: HashMap<CopyablePtr, DataCopy>,
    /// Information about currently in-progress reactions.
    reaction_stack: Vec<ReactionInfo<U>>,
}

impl<U: Universe> TransactionData<U> {
    pub fn viewpoint(&self) -> Clock {
        self.opaque.viewpoint
    }

    pub fn visibility(&self) -> Option<Clock> {
        Clock::new(self.opaque.visibility.load(Ordering::Relaxed))
    }

    pub fn finished(&self) -> bool {
        self.opaque.finished.load(Ordering::Relaxed)
    }

    pub fn state_mut<'a>(&'a self, write: &'a mut Unihandle<U>) -> &'a mut TransactionState<U> {
        self.opaque.state.write(write).get_mut().get_mut()
    }

    pub fn state<'a>(
        &'a self,
        read: &'a Unihandle<U>,
    ) -> impl std::ops::Deref<Target = RefCell<TransactionState<U>>> + 'a {
        self.opaque
            .state
            .read(read)
            .try_lock()
            .expect("concurrent uses of a single transaction")
    }
}

/// Skip all thread checks/locking and return the transaction-specific copy of the given data, if any.
/// Use this only for debugging purposes!
#[no_mangle]
#[doc(hidden)]
pub unsafe fn hornpipedbg_lookup_transaction_copy(
    tx: Option<DataPtr>,
    data: Option<DataPtr>,
) -> *mut DataFraming<UnknownData> {
    let Some(tx) = tx else {
        return std::ptr::null_mut();
    };
    let Some(data) = data else {
        return std::ptr::null_mut();
    };
    let tx = &*(crate::framing::hornpipedbg_deref(tx) as *const TransactionData<DefaultUniverse>);
    let state = (&mut *(&mut *tx.opaque.state.as_ptr()).data_ptr()).get_mut();
    match state.data_copies.get(&CopyablePtr::Data(data)) {
        Some(copy) => copy.ptr,
        None => std::ptr::null_mut(),
    }
}

impl<U: Universe> TransactionState<U> {
    pub fn next_step(&mut self) -> u32 {
        let current_step = self.next_step;
        self.next_step += 1;
        current_step
    }

    pub fn get_unknown_ref<'a>(
        &'a mut self,
        guard: &'a Guard,
        ptr: DataPtr,
    ) -> Option<&'a DataFraming<UnknownData>> {
        match self.data_copies.get(&CopyablePtr::Data(ptr)) {
            Some(o) => Some(o.as_ref()),
            None => ptr.load_frame(guard),
        }
    }

    pub fn get_ref<'a, T: Data>(
        &'a mut self,
        guard: &'a Guard,
        refer: Ref<T>,
    ) -> Option<&'a PartiallyOwned<T>> {
        // SAFETY: ...
        let (ptr, offset) = unsafe { Ref::as_raw(refer) };
        let ptr = ptr?;
        let copy_ptr = &self.get_unknown_ref(guard, ptr)?.data as *const UnknownData as *const u8;
        Some(unsafe { &*(copy_ptr.offset(offset as isize) as *const PartiallyOwned<T>) })
    }

    pub fn make_unknown_mut(
        &mut self,
        guard: &Guard,
        ptr: DataPtr,
    ) -> Option<&mut PartiallyOwned<DataFraming<UnknownData>>> {
        use std::collections::hash_map::Entry::*;

        let copy = match self.data_copies.entry(CopyablePtr::Data(ptr)) {
            Occupied(o) => o.into_mut(),
            Vacant(v) => {
                let frame = ptr.load_frame(guard)?;
                v.insert(DataCopy::copy_from(frame))
            }
        };
        Some(copy.make_mut())
    }

    pub fn make_mut<T: Data>(
        &mut self,
        guard: &Guard,
        refer: Ref<T>,
    ) -> Option<&mut PartiallyOwned<T>> {
        // SAFETY: ...
        let (ptr, offset) = unsafe { Ref::as_raw(refer) };
        let ptr = ptr?;
        let copy_ptr = self.make_unknown_mut(guard, ptr)?.data_mut();
        Some(unsafe { copy_ptr.to_known_partially_owned(offset as isize) })
    }
}

pub type TransactionOwn<U = DefaultUniverse> = Own<TransactionData<U>>;
pub type TransactionRef<U = DefaultUniverse> = Ref<TransactionData<U>>;

impl<U: Universe> TransactionRef<U> {
    pub fn viewpoint(&self, guard: &Guard) -> Clock {
        self.load(guard)
            .map(|this| this.viewpoint())
            .unwrap_or(STARTING_CLOCK)
    }
}

impl<U: Universe> PartialEq for TransactionRef<U> {
    fn eq(&self, other: &Self) -> bool {
        PtrCmp(*self) == PtrCmp(*other)
    }
}

impl<U: Universe> Eq for TransactionRef<U> {}

#[repr(C)]
pub struct GlobalInfo<U: Universe> {
    pub submitted: Own<TransactionData<U>>,
}

#[repr(C)]
pub struct LifecycleContext<'a, U: Universe = DefaultUniverse> {
    pub guard: &'a Guard,
    pub step: TransactionStepRef<U>,
}

impl<'a, U: Universe> LifecycleContext<'a, U> {
    pub fn fork(&mut self) -> LifecycleContext<'_, U> {
        LifecycleContext {
            guard: self.guard,
            step: self.step,
        }
    }
}

pub fn start_ref<U: Universe>() -> TransactionRef<U> {
    // - Load the global clock T into V, and increment
    // - Receive messages with visibility < V or order == V
    //    - If two messages conflict (overwrites, add+remove), use the one with largest order
    //    - Note visibility constraints on ignored messages (even if they don't yet exist) (TODO)
    // - Send messages with order=V and visibility=None
    // - V is the transaction viewpoint

    U::sequencer().update(|ordered_at, handle, seq| {
        let mut tx_data = TransactionState::<U>::default();
        tx_data.next_step = 1; // Step 0 is for isolation hack messages.
        let tx = TransactionData {
            opaque: Opaque::new(OpaqueTransactionData {
                viewpoint: ordered_at,
                visibility: AtomicU64::new(0),
                finished: AtomicBool::new(false),
                state: Unitex::new(ReentrantMutex::new(RefCell::new(tx_data))),
            }),
        };
        let tx = Own::new(tx);
        let weak_tx = tx.refer();
        seq.write(handle).transactions.push_back(tx);
        weak_tx
    })
}

pub enum SubmitResult {
    Submitted,
    NullTransaction,
}

pub fn submit_ref<U: Universe>(tx_ref: TransactionRef<U>) -> SubmitResult {
    let guard = pin();
    let Some(tx) = tx_ref.load(&guard) else {
        return SubmitResult::NullTransaction;
    };
    let viewpoint = tx.viewpoint();
    let seq = U::sequencer();
    {
        // Prepare everything before submit. This is a critical zone for submission, but other transactions can still read the global state.
        let handle = U::handle().upgradable_read();
        let state = tx.opaque.state.read(&handle).lock();

        let ctx = LifecycleContext {
            guard: &guard,
            step: TransactionStepRef {
                tx: tx_ref,
                step: state.borrow_mut().next_step(),
            },
        };
        let mut outgoing_copies: HashMap<_, _> = state
            .borrow_mut()
            .data_copies
            .iter_mut()
            .filter_map(|(k, internal)| {
                let ptr = k.clone();
                let value = match ptr {
                    CopyablePtr::Data(ptr) => {
                        let current = ptr.load_frame(&guard)?;
                        let mut outgoing = DataCopy::copy_from(current);
                        let outgoing_obj = outgoing.as_ref();
                        let ops = outgoing_obj.meta.operations;
                        let internal_mut = internal.make_mut().data_mut();
                        let outgoing_mut = outgoing.make_mut().data_mut();

                        // SAFETY: called with unmodified payload
                        unsafe {
                            ops.set_partial_ownership(
                                outgoing_mut,
                                OwnershipStatus::Copied as u8,
                                OwnershipStatus::Pending,
                            );

                            // merge the states to resolve conflicts
                            ops.finalize(
                                internal_mut,
                                outgoing_mut,
                                LifecycleContext {
                                    guard: ctx.guard,
                                    step: ctx.step,
                                },
                            );

                            ops.set_partial_ownership(
                                internal_mut,
                                OwnershipStatus::Pending as u8,
                                OwnershipStatus::Owned,
                            );
                            ops.set_partial_ownership(
                                outgoing_mut,
                                OwnershipStatus::Pending as u8,
                                OwnershipStatus::Owned,
                            );
                        }

                        outgoing
                    }
                };
                Some((ptr, value))
            })
            .collect();

        drop(state);

        // Do the actual submit. Do it fast because the whole universe will be locked.
        let mut owned_handle = parking_lot::RwLockUpgradableReadGuard::upgrade(handle);
        let handle = &mut owned_handle;
        let visible_at = seq.tick(handle);

        for (ptr, copy) in outgoing_copies.iter_mut() {
            match ptr {
                CopyablePtr::Data(ptr) => {
                    // SAFETY: well that is the whole of hornpipe, isn't it?
                    copy.ptr = unsafe { ptr.swap(copy.ptr) };
                }
            }
        }
        let isolation_copies = outgoing_copies;

        let other_txs: Vec<_> = seq
            .data
            .write(handle)
            .transactions
            .iter()
            .filter_map(|other_tx| {
                let other_tx_data = other_tx.load(&guard)?;
                if other_tx.refer() != tx_ref && other_tx_data.visibility().is_none() {
                    Some(other_tx_data)
                } else {
                    None
                }
            })
            .collect();

        for other_tx in other_txs {
            let other_tx_state = other_tx.opaque.state.write(handle).lock();
            for (ptr, copy) in isolation_copies.iter() {
                other_tx_state
                    .borrow_mut()
                    .data_copies
                    .entry(ptr.clone())
                    .or_insert(copy.make_user());
            }
        }

        // Update visibility
        tx.opaque
            .visibility
            .store(visible_at.into(), Ordering::Relaxed);

        // Now we can go back to letting other transactions see the global state.
        drop(owned_handle);

        // And now any misc intermediary state gets dropped.
    };
    SubmitResult::Submitted
}

pub fn finish_ref<U: Universe>(tx: TransactionRef<U>) {
    if let Some(tx) = tx.load(&pin()) {
        tx.opaque.finished.store(true, Ordering::Relaxed);
    }
}

// Consider this exampe:
// - Start with Init: op=Bar, visibility=0, order=0
// - Start with T=1
// Then we run a sequence of events:
// - Create A => viewpoint=1
// - A sends Foo => visibility=None, order=1
// - Create B => with viewpoint=2
// - B sends Bar => visibility=None, order=2
// - Commit B => change Bar to visibility=3
// - Commit A => change Foo to visibility=4
//
// "Serializing" means:
// - Events are reordered so that all from a particulartransaction are contiguous
// - The relative order of messages must be the same, as this determines the observed state
// - When recieving, messages are ignored/visible according to the transaction rules
//
// Currently we can order the events without any such conflict. For "A then B" gives:
// - Create A => viewpoint=1
// - A sends Foo => visibility=None, order=1
// - Commit A => change Foo to visibility=2
// - Create B => viewpoint=3
// - B sends Bar => visibility=None, order=3
// - Commit B => change Bar to visibility=4
//
// But now we will add some recieve events and consider serializations:
// - Create A => viewpoint=1
// - A sends Foo => visibility=None, order=1
// - Create B => viewpoint=2
// - B recieves Init => event with visibility < 2
// - B sends Bar => visibility=None, order=2
// - Commit B => change Bar to visibility=3
// - Commit A => change Foo to visibility=4
//
// With "B then A":
// - Create B => viewpoint=1
// - B recieves Init => event with visibility < 1
// - B sends Bar => visibility=None, order=1
// - Commit B => change Bar to visibility=2
// - Create A => viewpoint=3
// - A sends Foo => visibility=None, order=3
// - Commit A => change Foo to visibility=4
// ILLEGAL: would require the relative ordering "Bar then Foo"
//
// With "A then B":
// - Create A => viewpoint=1
// - A sends Foo => visibility=None, order=1
// - Commit A => change Foo to visibility=2
// - Create B => viewpoint=3
// - B recieves Init => event with visibility < 3
// - B sends Bar => visibility=None, order=3
// - Commit B => change Bar to visibility=4
// ILLEGAL: would require "B recieves Foo => event with visibility < 3"
//
// Becasue there is no valid serialization of A and B, transaction A should be unable to commit.
// This may be tricky to dectect, but there are a few rules we should follow:
// - Only consider the serialization which is sorted by commit viewpoint, "A then B" in
//   this case. Any other serializations can be ignored because they are likely to change
//   relative sent message ordering.
//    - We might want to implement "rebasing" later, where a different serialization
//      order is chosen based on disjoint send relationships
// - When B commits, it could require any ignored messages (Foo) to have visibility > 2.
//   Transaction A would discover that constraint is violated when it goes to commit.
// - This is harder if Foo shows up after B would have ignored it. The entire slot could
//   have a "I ignored you because you show up later" constraint. Is this too strict?
//    - Ignored overwrite messages can gain visibility late without conflict, so long as
//      another overwrite would cause them to be ignored in any case
//    - Messages on conflict-free datatypes can always gain visibility late
//
// Also notice that it does not matter what order commits are commited in, since
// order is already espablished. That only decides which commit should be blamed.
