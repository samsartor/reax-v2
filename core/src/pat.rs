// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//! This module defines utility types and functions implementing common object patterns.

use crate::messages::OnDirty;
use crate::{
    create_now_in, logic, Element, KeyElement, ObjectOwn, ObjectRef, Operation, Reactor,
    SlotElement, SlotId, Transaction, TransactionRef, Universe,
};
use crossbeam_epoch::pin;
use smallvec::SmallVec;
use std::fmt;
use std::marker::PhantomData;
use std::panic::{catch_unwind, AssertUnwindSafe};
#[cfg(feature = "tracing")]
use tracing::trace;

pub struct FnComputer<F, U> {
    function: F,
    universe: PhantomData<U>,
}

impl<F, U> fmt::Debug for FnComputer<F, U> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("FnComputer").finish_non_exhaustive()
    }
}

impl<F, U> FnComputer<F, U> {
    pub fn new(function: F) -> Self {
        FnComputer {
            function,
            universe: PhantomData,
        }
    }
}

impl<F, U: Universe> FnComputer<F, U>
where
    F: Fn(&Transaction<U>, ObjectRef<U>) -> Element<U> + Sync + Send + 'static,
{
    pub fn slot(function: F) -> SlotElement<U> {
        SlotElement::Reactor(Box::new(Self::new(function)))
    }
}

impl<F, U: Universe> Reactor<U> for FnComputer<F, U>
where
    F: Fn(&Transaction<U>, ObjectRef<U>) -> Element<U> + Sync + Send + 'static,
{
    fn react(&self, tx: TransactionRef<U>, into: SlotId<U>) {
        // TODO: check if computation is actually needed
        let tx = Transaction::<U>::resume(tx);
        logic::start_computation(tx.refer(), tx.guard(), into);
        let (panic, ops) = match catch_unwind(AssertUnwindSafe(|| (self.function)(&tx, into.ptr))) {
            Ok(value) => (None, value.to_op()),
            Err(panic) => (Some(panic), Operation::NoOp),
        };
        logic::finish_computation(tx.refer(), tx.guard(), &mut [ops]);
        if let Some(panic) = panic {
            if let Some(msg) = panic.downcast_ref::<&'static str>() {
                eprintln!("panic while recomputing {into:?}: {msg:?}")
            } else if let Some(msg) = panic.downcast_ref::<String>() {
                eprintln!("panic while recomputing {into:?}: {msg:?}")
            } else {
                eprintln!("panic while recomputing {into:?}")
            }
        }
    }

    fn pipeline(
        &self,
        _tx: TransactionRef<U>,
        _source: SlotId<U>,
        _operations: &[Operation<U>],
        _into: SlotId<U>,
    ) {
    }
}

#[derive(Debug)]
pub struct CollectPipeline;

impl CollectPipeline {
    pub fn slot<U: Universe>() -> SlotElement<U> {
        SlotElement::Reactor(Box::new(Self))
    }
}

impl<U: Universe> Reactor<U> for CollectPipeline {
    fn react(&self, _tx: TransactionRef<U>, _into: SlotId<U>) {}

    fn pipeline(
        &self,
        tx: TransactionRef<U>,
        _source: SlotId<U>,
        operations: &[Operation<U>],
        into: SlotId<U>,
    ) {
        todo!()
        /*
        // TODO: is there a general way to avoid heap allocation here?
        let mut collected: SmallVec<[_; 8]> =
            operations.iter().filter_map(|op| op.copy()).collect();
        logic::resolve_slot(tx, &pin(), into, &mut collected);
        */
    }
}

pub struct PureMapPipeline<F, U> {
    function: F,
    universe: PhantomData<U>,
}

impl<F, U> fmt::Debug for PureMapPipeline<F, U> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("PureMapPipeline").finish_non_exhaustive()
    }
}

impl<F, U> PureMapPipeline<F, U> {
    pub fn new(function: F) -> Self {
        PureMapPipeline {
            function,
            universe: PhantomData,
        }
    }
}

impl<F, U: Universe> PureMapPipeline<F, U>
where
    F: Fn(&Transaction<U>, Element<U>) -> Element<U> + Sync + Send + 'static,
{
    pub fn slot(function: F) -> SlotElement<U> {
        SlotElement::Reactor(Box::new(Self::new(function)))
    }
}

impl<F, U: Universe> Reactor<U> for PureMapPipeline<F, U>
where
    F: Fn(&Transaction<U>, Element<U>) -> Element<U> + Sync + Send + 'static,
{
    fn react(&self, _tx: TransactionRef<U>, _into: SlotId<U>) {}

    fn pipeline(
        &self,
        tx: TransactionRef<U>,
        _source: SlotId<U>,
        operations: &[Operation<U>],
        into: SlotId<U>,
    ) {
        todo!()
        /*
        // TODO: is there a general way to avoid heap allocation here?
        let tx = Transaction::resume(tx);
        let mut collected: SmallVec<[_; 8]> = operations
            .iter()
            .filter_map(|op| match op {
                Operation::Overwrite(value) => {
                    Some(Operation::Overwrite((self.function)(&tx, value.refer())))
                }
                Operation::AddElement { key, value } => Some(Operation::AddElement {
                    key: key.clone(),
                    value: (self.function)(&tx, value.refer()),
                }),
                Operation::RemoveElement { key } => {
                    Some(Operation::RemoveElement { key: key.clone() })
                }
                _ => None,
            })
            .collect();
        logic::resolve_slot(tx.refer(), tx.guard(), into, &mut collected);
        */
    }
}

pub fn create_filter_map<U: Universe>(
    tx: TransactionRef<U>,
    source: SlotId<U>,
    function: impl Fn(&Transaction<U>, ObjectRef<U>, Element<U>) -> Option<Element<U>>
        + Clone
        + 'static
        + Send
        + Sync,
) -> ObjectOwn<U> {
    // The internals of this function are pretty complex, and rely on the exact nitty-gritty
    // behavior of hornpipe_core in so many ways. In a sense, this may be the most complex single
    // user of the library. Be careful!

    // Responsible for eagerly taking elements out of the input collection and putting them
    // in wrapper objects to do the actual reaction.
    struct FilterMapInputPipeline<F>(F);

    impl<F> fmt::Debug for FilterMapInputPipeline<F> {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            f.debug_struct("FilterMapInputPipeline")
                .finish_non_exhaustive()
        }
    }

    impl<F, U: Universe> Reactor<U> for FilterMapInputPipeline<F>
    where
        F: 'static + Clone + Sync + Send,
        FilterMapReactor<F, U>: Reactor<U>,
    {
        fn react(&self, _tx: TransactionRef<U>, _into: SlotId<U>) {}

        fn pipeline(
            &self,
            tx: TransactionRef<U>,
            source: SlotId<U>,
            operations: &[Operation<U>],
            r_slot: SlotId<U>,
        ) {
            todo!()
            /*
            let o_slot = SlotId {
                ptr: r_slot.ptr,
                slot: 0,
            };
            #[cfg(feature = "tracing")]
            trace!(target: "filter_map_input_pipeline", source=?source, dest=?o_slot, operations=?operations);
            let mut o_collected: SmallVec<[_; 1]> = SmallVec::new();
            let mut r_collected: SmallVec<[_; 1]> = SmallVec::new();

            let guard = pin();
            for op in operations {
                match op {
                    // Addition from the original collection: create a new reactor object.
                    Operation::AddElement { key, value } => {
                        let reactor = create_now_in(
                            U::this(),
                            &[],
                            [
                                value.refer().slot(),
                                Element::Null.slot(),
                                SlotElement::Reactor(Box::new(FilterMapReactor(
                                    key.clone(),
                                    self.0.clone(),
                                ))),
                            ],
                        );
                        reactor.store_label(format_args!("reactor for {:?}", value));
                        logic::resolve_slot(
                            tx,
                            &guard,
                            reactor.slot_id(1),
                            &mut [Operation::AddDownstream(
                                o_slot,
                                OnDirty::ADD_UPSTREAM | OnDirty::RUN_PIPELINE,
                            )],
                        );
                        r_collected.push(Operation::AddElement {
                            key: key.clone(),
                            value: Element::Own(reactor),
                        });
                    }
                    // Removal via the original collection: delete both the reactor object and output object.
                    Operation::RemoveElement { key } => {
                        r_collected.push(Operation::RemoveElement { key: key.clone() });
                        o_collected.push(Operation::RemoveElement { key: key.clone() });
                    }
                    _ => (),
                }
            }

            logic::resolve_slot(tx, &guard, o_slot, &mut o_collected);
            logic::resolve_slot(tx, &guard, r_slot, &mut r_collected);
            */
        }
    }

    // Responsible for eagerly taking elements out of the wrapper objects containing the reactors,
    // and putting them in the actual output collection.
    struct FilterMapOutputPipeline;

    impl fmt::Debug for FilterMapOutputPipeline {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            f.debug_struct("FilterMapOutputPipeline")
                .finish_non_exhaustive()
        }
    }

    impl<U: Universe> Reactor<U> for FilterMapOutputPipeline {
        fn react(&self, tx: TransactionRef<U>, into: SlotId<U>) {
            todo!()
            /*
            logic::resolve_slot(tx, &pin(), into, &mut [Operation::ClearUpstreams]);
            */
        }

        fn pipeline(
            &self,
            tx: TransactionRef<U>,
            #[allow(dead_code)] _source: SlotId<U>,
            operations: &[Operation<U>],
            o_slot: SlotId<U>,
        ) {
            todo!()
            /*
            #[cfg(feature = "tracing")]
            trace!(target: "filter_map_output_pipeline", source=?_source, dest=?o_slot, operations=?operations);
            let mut collected: SmallVec<[_; 1]> = SmallVec::new();

            for op in operations {
                match op {
                    // Addition from some reactor object: add to the output object.
                    Operation::AddElement { key, ref value } => {
                        collected.push(Operation::AddElement {
                            key: key.clone(),
                            value: value.refer(),
                        });
                    }
                    // Removal via some reactor object: leave the reactor in place.
                    Operation::RemoveElement { key } => {
                        collected.push(Operation::RemoveElement { key: key.clone() });
                    }
                    _ => (),
                }
            }

            if !collected.is_empty() {
                logic::resolve_slot(tx, &pin(), o_slot, &mut collected);
            }
            */
        }
    }

    impl<F, U: Universe> fmt::Debug for FilterMapReactor<F, U> {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            f.debug_struct("FilterMapReactor").finish_non_exhaustive()
        }
    }

    struct FilterMapReactor<F, U: Universe>(KeyElement<U>, F);

    impl<F, U: Universe> Reactor<U> for FilterMapReactor<F, U>
    where
        F: Fn(&Transaction<U>, ObjectRef<U>, Element<U>) -> Option<Element<U>>,
    {
        fn react(&self, tx: TransactionRef<U>, into: SlotId<U>) {
            todo!()
            /*
            #[cfg(feature = "tracing")]
            trace!(target: "filter_map_react_pipeline", into=?into);
            let tx = Transaction::resume(tx);
            logic::start_computation(tx.refer(), tx.guard(), into);
            let (panic, op) =
                if let Some(el) = logic::get_slot(tx.refer(), tx.guard(), into.ptr.slot_id(0)) {
                    match catch_unwind(AssertUnwindSafe(|| (self.1)(&tx, into.ptr, el))) {
                        Ok(Some(value)) => (
                            None,
                            Operation::AddElement {
                                key: self.0.clone(),
                                value,
                            },
                        ),
                        Ok(None) => (
                            None,
                            Operation::RemoveElement {
                                key: self.0.clone(),
                            },
                        ),
                        Err(panic) => (Some(panic), Operation::NoOp),
                    }
                } else {
                    (None, Operation::NoOp)
                };
            logic::finish_computation(tx.refer(), tx.guard(), &mut [op]);
            if let Some(panic) = panic {
                if let Some(msg) = panic.downcast_ref::<&'static str>() {
                    eprintln!("panic while filtering/mapping within {into:?}: {msg:?}")
                } else if let Some(msg) = panic.downcast_ref::<String>() {
                    eprintln!("panic while filtering/mapping within  {into:?}: {msg:?}")
                } else {
                    eprintln!("panic while filtering/mapping whithin  {into:?}")
                }
            }
            */
        }

        fn pipeline(
            &self,
            _tx: TransactionRef<U>,
            _source: SlotId<U>,
            _operations: &[Operation<U>],
            _target: SlotId<U>,
        ) {
        }
    }

    let dest = create_now_in(
        U::this(),
        (),
        [
            Element::Null.slot(),
            SlotElement::Reactor(Box::new(FilterMapOutputPipeline)),
            Element::Null.slot(),
            SlotElement::Reactor(Box::new(FilterMapInputPipeline(function))),
        ],
    );
    // TODO: apply pipeline to current state
    logic::write_slot(
        tx,
        &pin(),
        source,
        None,
        &mut [
            Operation::AddDownstream(dest.slot_id(0), OnDirty::empty()),
            Operation::AddDownstream(dest.slot_id(2), OnDirty::RUN_PIPELINE),
        ],
    );
    dest
}
