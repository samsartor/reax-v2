//! This is a fork of stackmap-1.0.5 from crates.io with a few important changes:
//! - the Ord requirement is replaced with Key
//! - methods are added for PartiallyOwned<StackMap>
//! - a CopyRef impl is added

use super::Keyed;
use crate as hornpipe_core;
use crate::share::{CopyRef, PartiallyOwned};
use crossbeam_epoch::Guard;
use hornpipe_macros::ProjectPartiallyOwned;
use std::borrow::Cow;
use std::fmt;
use std::mem::{replace, MaybeUninit};

#[derive(CopyRef, ProjectPartiallyOwned)]
pub struct KVPair<K: Keyed, V: CopyRef + Send + Sync + 'static> {
    key: K,
    pub value: V,
}

impl<K: Keyed, V: CopyRef + Send + Sync + 'static> Keyed for KVPair<K, V> {
    type Key = K::Key;

    fn key(&self, guard: &Guard) -> Cow<K::Key> {
        self.key.key(guard)
    }
}

/// `StackMap` is a constant-size, zero-allocation associative container
/// backed by an array. It can be used as a building block for various interesting
/// higher-level data structures.
pub struct StackMap<T: Keyed, const FANOUT: usize> {
    len: usize,
    inner: [MaybeUninit<T>; FANOUT],
}

impl<T: Keyed + fmt::Debug, const FANOUT: usize> fmt::Debug for StackMap<T, FANOUT> {
    fn fmt(&self, w: &mut fmt::Formatter<'_>) -> fmt::Result {
        w.debug_struct(&format!("StackMap<{}>", FANOUT)).finish()?;
        w.debug_set().entries(self.iter()).finish()
    }
}

impl<T: Keyed, const FANOUT: usize> Drop for StackMap<T, FANOUT> {
    fn drop(&mut self) {
        for i in 0..self.len() {
            let ptr = self.inner[i].as_mut_ptr();
            unsafe {
                std::ptr::drop_in_place(ptr);
            }
        }
    }
}

impl<T: Keyed, const FANOUT: usize> Default for StackMap<T, FANOUT> {
    #[inline]
    fn default() -> Self {
        StackMap::new()
    }
}

impl<T: Keyed, const FANOUT: usize> StackMap<T, FANOUT> {
    pub const fn new() -> Self {
        Self {
            inner: unsafe { MaybeUninit::<[MaybeUninit<_>; FANOUT]>::uninit().assume_init() },
            len: 0,
        }
    }

    fn binary_search(&self, key: &T::Key, guard: &Guard) -> Result<usize, usize> {
        self.inner[..self.len()].binary_search_by_key(&Cow::Borrowed(key), |slot| unsafe {
            slot.assume_init_ref().key(guard)
        })
    }

    pub fn get(&self, key: &T::Key, guard: &Guard) -> Option<&T> {
        if let Ok(index) = self.binary_search(key, guard) {
            Some(unsafe { &self.inner.get_unchecked(index).assume_init_ref() })
        } else {
            None
        }
    }

    /// Inserts an item and return the previous value if it exists.
    ///
    /// # Panics
    ///
    /// This method will panic if called with a new key-value pair when
    /// already full.
    ///
    /// The `StackMap` should be checked to ensure that it is not already
    /// full before calling this method. It is full when the `self.is_full()`
    /// method returns `true`, which happens when `self.len() == FANOUT`.
    pub fn insert(&mut self, item: T, guard: &Guard) -> Option<T> {
        match self.binary_search(&item.key(guard), guard) {
            Ok(index) => {
                let slot = unsafe { &mut self.inner.get_unchecked_mut(index).assume_init_mut() };
                Some(std::mem::replace(slot, item))
            }
            Err(index) => {
                assert!(self.len() < FANOUT);

                unsafe {
                    if index < self.len() {
                        let src = self.inner.get_unchecked(index).as_ptr();
                        let dst = self.inner.get_unchecked_mut(index + 1).as_mut_ptr();

                        std::ptr::copy(src, dst, self.len() - index);
                    }

                    self.len += 1;
                    self.inner.get_unchecked_mut(index).write(item);
                }
                None
            }
        }
    }

    pub fn remove<Q>(&mut self, key: &T::Key, guard: &Guard) -> Option<T> {
        if let Ok(index) = self.binary_search(key, guard) {
            unsafe {
                let ret = std::ptr::read(self.inner.get_unchecked(index).as_ptr());

                if index + 1 < self.len() {
                    let src = self.inner.get_unchecked(index + 1).as_ptr();
                    let dst = self.inner.get_unchecked_mut(index).as_mut_ptr();

                    std::ptr::copy(src, dst, self.len() - index);
                }

                self.len -= 1;

                Some(ret)
            }
        } else {
            None
        }
    }

    pub fn contains_key(&self, key: &T::Key, guard: &Guard) -> bool {
        self.binary_search(key, guard).is_ok()
    }

    pub fn iter(&self) -> impl DoubleEndedIterator<Item = &T> {
        self.inner[..self.len()]
            .iter()
            .map(|slot| unsafe { slot.assume_init_ref() })
    }

    pub fn iter_mut<'a>(&'a mut self) -> impl DoubleEndedIterator<Item = &'a mut T> {
        let len = self.len();
        self.inner[..len]
            .iter_mut()
            .map(|slot| unsafe { slot.assume_init_mut() })
    }

    /// Splits this `StackMap` into two. `self` will retain
    /// all key-value pairs before the provided split index.
    /// Returns a new `StackMap` created out of all key-value pairs
    /// at or after the provided split index.
    pub fn split_off(&mut self, split_idx: usize) -> Self {
        assert!(split_idx < self.len());
        assert!(split_idx < FANOUT);

        let mut rhs = Self::default();

        for i in split_idx..self.len() {
            let src = self.inner[i].as_ptr();
            let dst = rhs.inner[i - split_idx].as_mut_ptr();
            unsafe {
                std::ptr::copy_nonoverlapping(src, dst, 1);
            }
        }

        rhs.len = self.len - split_idx;
        self.len = split_idx;

        rhs
    }

    /// Get a key-value pair based on its internal relative
    /// index in the backing array.
    pub fn get_index(&self, index: usize) -> Option<&T> {
        if index < self.len() {
            Some(unsafe { self.inner.get_unchecked(index).assume_init_ref() })
        } else {
            None
        }
    }

    /// Get the key-value pair that is less than or equal
    /// to the provided key. Useful for any least upper
    /// bound operation, such as MVCC lookups where the
    /// key is suffixed by a version or an internal b-tree
    /// index lookup where you are looking for the next
    /// node based on a node's low key.
    ///
    /// # Examples
    /// ```
    /// let mut map = stack_map::StackMap::<u8, u8, 64>::default();
    /// map.insert(1, 1);
    /// map.insert(2, 2);
    /// map.insert(3, 3);
    ///
    /// let lt = map.get_less_than_or_equal(&4).unwrap();
    /// let expected = &(3, 3);
    /// assert_eq!(expected, lt);
    ///
    /// let lt = map.get_less_than_or_equal(&3).unwrap();
    /// let expected = &(3, 3);
    /// assert_eq!(expected, lt);
    ///
    /// let lt = map.get_less_than_or_equal(&2).unwrap();
    /// let expected = &(2, 2);
    /// assert_eq!(expected, lt);
    ///
    /// let lt = map.get_less_than_or_equal(&1).unwrap();
    /// let expected = &(1, 1);
    /// assert_eq!(expected, lt);
    ///
    /// let lt = map.get_less_than_or_equal(&0);
    /// let expected = None;
    /// assert_eq!(expected, lt);
    /// ```
    pub fn get_less_than_or_equal(&self, key: &T::Key, guard: &Guard) -> Option<&T> {
        // binary search LUB
        let index = match self.binary_search(key, guard) {
            Ok(i) => i,
            Err(0) => return None,
            Err(i) => i - 1,
        };

        self.get_index(index)
    }

    pub fn get_less_than_or_equal_or_first(&self, key: &T::Key, guard: &Guard) -> Option<&T> {
        // binary search LUB
        let index = match self.binary_search(key, guard) {
            Ok(i) => i,
            Err(0) => 0,
            Err(i) => i - 1,
        };

        self.get_index(index)
    }

    /// Gets a kv pair that has a key that is less than the provided key.
    ///
    /// # Examples
    /// ```
    /// let mut map = stack_map::StackMap::<u8, u8, 64>::default();
    /// map.insert(1, 1);
    /// map.insert(2, 2);
    /// map.insert(3, 3);
    ///
    /// let lt = map.get_less_than(&4).unwrap();
    /// let expected = &(3, 3);
    /// assert_eq!(expected, lt);
    ///
    /// let lt = map.get_less_than(&3).unwrap();
    /// let expected = &(2, 2);
    /// assert_eq!(expected, lt);
    ///
    /// let lt = map.get_less_than(&2).unwrap();
    /// let expected = &(1, 1);
    /// assert_eq!(expected, lt);
    ///
    /// let lt = map.get_less_than(&1);
    /// let expected = None;
    /// assert_eq!(expected, lt);
    ///
    /// let lt = map.get_less_than(&0);
    /// let expected = None;
    /// assert_eq!(expected, lt);
    /// ```
    pub fn get_less_than<Q>(&self, key: &T::Key, guard: &Guard) -> Option<&T> {
        // binary search LUB
        let index = match self.binary_search(key, guard) {
            Ok(0) | Err(0) => return None,
            Ok(i) => i - 1,
            Err(i) => i - 1,
        };

        self.get_index(index)
    }

    /// Returns the first kv pair in the StackMap, if any exists
    ///
    /// # Examples
    /// ```
    /// let mut sm = stack_map::StackMap::<u8, u8, 3>::default();
    /// sm.insert(1, 1);
    /// sm.insert(2, 2);
    /// sm.insert(3, 3);
    ///
    /// let expected = Some(&(1, 1));
    /// let actual = sm.first();
    /// assert_eq!(expected, actual);
    /// ```
    pub fn first(&self) -> Option<&T> {
        self.get_index(0)
    }

    /// Returns the last kv pair in the StackMap, if any exists
    ///
    /// # Examples
    /// ```
    /// let mut sm = stack_map::StackMap::<u8, u8, 3>::default();
    /// sm.insert(1, 1);
    /// sm.insert(2, 2);
    /// sm.insert(3, 3);
    ///
    /// let expected = Some(&(3, 3));
    /// let actual = sm.last();
    /// assert_eq!(expected, actual);
    /// ```
    pub fn last(&self) -> Option<&T> {
        if self.is_empty() {
            None
        } else {
            self.get_index(self.len - 1)
        }
    }

    /// Returns `true` if this `StackMap` is at its maximum capacity and
    /// unable to receive additional data.
    ///
    /// # Examples
    /// ```
    /// let mut sm = stack_map::StackMap::<u8, u8, 3>::default();
    /// sm.insert(1, 1);
    /// sm.insert(2, 2);
    /// sm.insert(3, 3);
    ///
    /// let expected = true;
    /// let actual = sm.is_full();
    /// assert_eq!(expected, actual);
    /// ```
    pub const fn is_full(&self) -> bool {
        self.len == FANOUT
    }

    pub const fn len(&self) -> usize {
        self.len
    }

    pub const fn is_empty(&self) -> bool {
        self.len == 0
    }
}

// These are new hornpipe-specific additions to the API
// ====================================================

unsafe impl<T: Keyed, const FANOUT: usize> CopyRef for StackMap<T, FANOUT> {
    unsafe fn partial_drop(this: &mut PartiallyOwned<Self>) {
        let len = this.len;
        unsafe {
            for x in &mut this.as_mut().inner[..len] {
                let x = x.assume_init_mut();
                T::partial_drop(PartiallyOwned::new_mut(x));
            }
        }
    }

    unsafe fn set_partial_ownership(
        this: &mut PartiallyOwned<Self>,
        matching: u8,
        status: crate::share::OwnershipStatus,
    ) {
        let len = this.len;
        unsafe {
            for x in &mut this.as_mut().inner[..len] {
                let x = x.assume_init_mut();
                T::set_partial_ownership(PartiallyOwned::new_mut(x), matching, status);
            }
        }
    }

    fn visit_owned(this: &Self, callback: &mut (impl FnMut(crate::framing::DataPtr) + ?Sized)) {
        for x in this.iter() {
            T::visit_owned(x, callback);
        }
    }
}

impl<'owner, T: Keyed, const FANOUT: usize> StackMap<T, FANOUT> {
    pub fn insert_po(
        self: &mut PartiallyOwned<'owner, Self>,
        item: PartiallyOwned<'owner, T>,
        guard: &Guard,
    ) -> Option<PartiallyOwned<'owner, T>> {
        match self.binary_search(&item.key(guard), guard) {
            Ok(index) => {
                // In this case we need to make sure we use replace_with_owned.

                let slot = unsafe {
                    PartiallyOwned::new_mut(
                        self.as_mut()
                            .inner
                            .get_unchecked_mut(index)
                            .assume_init_mut(),
                    )
                };
                Some(replace(slot, item))
            }
            Err(index) => {
                // This case does not need any special handling, because no values are being overwritten or ownership changed.
                // Just keep the same code as insert, for the most part.

                assert!(self.len() < FANOUT);

                unsafe {
                    let this = self.as_mut();
                    if index < this.len() {
                        let src = this.inner.get_unchecked(index).as_ptr();
                        let dst = this.inner.get_unchecked_mut(index + 1).as_mut_ptr();

                        std::ptr::copy(src, dst, this.len() - index);
                    }

                    this.len += 1;

                    PartiallyOwned::new_ptr(this.inner.get_unchecked_mut(index).as_mut_ptr())
                        .write(item);
                }
                None
            }
        }
    }

    pub fn remove_po(
        self: &mut PartiallyOwned<'owner, Self>,
        key: &T::Key,
        guard: &Guard,
    ) -> Option<PartiallyOwned<T>> {
        if let Ok(index) = self.binary_search(key, guard) {
            unsafe {
                let this = self.as_mut();

                let ret =
                    PartiallyOwned::wrap(std::ptr::read(this.inner.get_unchecked(index).as_ptr()));

                if index + 1 < this.len() {
                    let src = this.inner.get_unchecked(index + 1).as_ptr();
                    let dst = this.inner.get_unchecked_mut(index).as_mut_ptr();

                    std::ptr::copy(src, dst, this.len() - index);
                }

                this.len -= 1;

                Some(ret)
            }
        } else {
            None
        }
    }
}
