// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//! This module is responsible for garbage collecting old messages inside slots.

use crate::Universe;

//#[cfg_attr(feature = "tracing", tracing::instrument(level = "trace"))]
pub fn finalize<U: Universe>() {
    // !!! THIS DOCUMENTATION IS SUPER OUT-OF-DATE !!!
    // (I'm keeping it here for reference while I'm still thinking about this problem)
    //
    // The job of this function is to free up old messages left in mailboxes and old transaction
    // metadata left in the sequencer.
    //
    // - Messages without visibility must be kept alive so long as the sending transaction is
    // unfinished. When a transaction is finished without gaining visibility (i.e. canceled),
    // all its messages must be freed.
    //
    // - A message with visibility s must be kept alive so long as either:
    //     1. a transaction exists with viewpoint v < s (TODO: relax this?)
    //     2. the message establishes state (e.g. is the most recent Overwrite*)
    //
    // We want to aviod reviewing all messages whenever a transaction is dropped. Instead, we can
    // can find all messages with a visibility s though the transaction with that visibility.
    //
    // When a transaction is dropped AND the next transaction is already visible, the next
    // transaction's messages should be reduced (either freed outright or merged with other messages).
    //
    // All unfinished transactions must be kept. Finished transactions must be kept if they maintain
    // some message metadata needed to establish one of the earlier conditions.

    // ------------------------------
}
