// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//! Broadly speaking this module is responsible for maintaining awareness
//! between transactions. Right now this is achived in the simplest way:
//! - Transactions are strongly ordered by clock integers

use super::{TransactionOwn, TransactionRef};
use crate::framing::DataPtr;
use crate::utex::{Unihandle, Unitex};
use crate::{DefaultUniverse, Universe};
use crossbeam_epoch::Guard;
use crossbeam_queue::SegQueue;
use once_cell::unsync::Lazy;
use std::{collections::VecDeque, mem::replace, num::NonZeroU64};

pub struct SequencerData<U: Universe> {
    pub clock: Clock,
    pub transactions: VecDeque<TransactionOwn<U>>,
}

pub struct Sequencer<U: Universe> {
    pub(crate) dropped: SegQueue<DataPtr>,
    pub(crate) data: Unitex<Lazy<SequencerData<U>>, U>,
}

impl<U: Universe> Sequencer<U> {
    pub const fn new() -> Self {
        Sequencer {
            dropped: SegQueue::new(),
            data: Unitex::new(Lazy::new(|| SequencerData {
                clock: STARTING_CLOCK
                    .checked_add(1)
                    .expect("hornpipe exausted its runtime (please file a bug)"),
                transactions: VecDeque::new(),
            })),
        }
    }

    pub fn tick(&self, handle: &mut Unihandle<U>) -> Clock {
        let data = self.data.write(handle);
        let next = data.clock.checked_add(1).unwrap();
        replace(&mut data.clock, next)
    }

    /// Recieve exclusive access the sequencer and a timestamp uniquely identifying that
    /// period of access relative to other periods.
    pub fn update<T>(
        &self,
        func: impl FnOnce(Clock, &mut Unihandle<U>, &Unitex<SequencerData<U>, U>) -> T,
    ) -> T {
        let mut handle = U::handle().write();
        let clock = self.tick(&mut handle);
        let unlazy = self.data.map_write(&mut *handle, |data| &mut **data);
        func(clock, &mut handle, unlazy)
    }

    /// Destroy all live transactions whether or not they are complete. Any attempt to
    /// use a destroyed transaction will panic, so generally avoid doing this ever.
    /// Its only actual job is to provide certain unit tests with a clean slate.
    #[allow(dead_code)]
    pub(crate) fn hard_reset(&self) {
        let mut handle = U::handle().write();
        self.data.write(&mut *handle).transactions.clear();
    }
}

/// Uniquely identifies an atomic instant in the transaction history. Most transactions
/// have two such instants: viewpoint and visability. See the /doc directory for more.
pub type Clock = NonZeroU64;

/// Any operation marked with this instant is treated as having always happened.
pub const STARTING_CLOCK: Clock = unsafe { Clock::new_unchecked(1) };

/// Used to completely order operations relative to other operations.
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[repr(C)]
pub struct TransactionStepRef<U: Universe = DefaultUniverse> {
    /// Used to query the visibility at this exact instant.
    pub tx: TransactionRef<U>,
    /// The relative offset of the operation within its transaction.
    pub step: u32,
}

impl<U: Universe> TransactionStepRef<U> {
    pub const fn eternal() -> Self {
        TransactionStepRef {
            tx: TransactionRef::null(),
            step: 0,
        }
    }

    pub fn tx(self) -> TransactionRef<U> {
        self.tx
    }

    pub fn step(self) -> u32 {
        self.step
    }

    /// The current visibility and viewpoint of this operation. Be aware that a visibility
    /// reported as None may change at any time.
    pub fn clocks(&self, guard: &Guard) -> (Option<Clock>, Clock) {
        match *self {
            TransactionStepRef { tx, .. } => {
                let Some(tx) = tx.load(guard) else {
                    // Finalization should remove all references to a dead transaction
                    // from messages, but if there is a fault in that somewhere just keep
                    // the message hidden.
                    debug_assert!(false);
                    return (None, STARTING_CLOCK);
                };
                (tx.visibility(), tx.viewpoint())
            }
        }
    }

    /// Is the operation visible from the given viewpoint.
    #[inline]
    pub fn is_visible(&self, viewpoint: Clock, guard: &Guard) -> bool {
        let (this_visibility, this_viewpoint) = self.clocks(guard);
        match this_visibility {
            Some(visible_at) => visible_at <= viewpoint,
            None => this_viewpoint == viewpoint,
        }
    }

    /// The tuple used for ordering.
    pub fn pair(&self, guard: &Guard) -> (Clock, u32) {
        match *self {
            TransactionStepRef { ref tx, step } => (tx.viewpoint(guard), step),
        }
    }
}

impl<U: Universe> Ord for TransactionStepRef<U> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        let guard = crossbeam_epoch::pin();
        self.pair(&guard).cmp(&other.pair(&guard))
    }
}

impl<U: Universe> PartialOrd for TransactionStepRef<U> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}
