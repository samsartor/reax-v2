// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use crate::data::{Data, Operations};
use crate::framing::{DataFraming, DataPtr, Indirection, UnknownData};
use crate::share::CopyRef;
use crate::transaction::{TransactionData, TransactionRef};
use crate::{Transaction, Universe};
use crossbeam_epoch::{pin, Guard};
use std::any::type_name;
use std::fmt;
use std::hash::{Hash, Hasher};
use std::hint::unreachable_unchecked;
use std::marker::PhantomData;
use std::ops::Deref;
use std::{cmp, ptr};

/// A weak reference to a hornpipe object.
#[repr(C)]
pub struct Ref<T> {
    raw: Option<DataPtr>,
    off: u32,
    typ: PhantomData<Box<T>>,
}

impl<T> Copy for Ref<T> {}
impl<T> Clone for Ref<T> {
    #[inline]
    fn clone(&self) -> Self {
        *self
    }
}

impl<T> Ref<T> {
    #[inline]
    pub const unsafe fn from_raw(raw: DataPtr, off: u32) -> Self {
        Ref {
            raw: Some(raw),
            off,
            typ: PhantomData,
        }
    }

    #[inline]
    pub const unsafe fn as_raw(this: Self) -> (Option<DataPtr>, u32) {
        (this.raw, this.off)
    }

    #[inline]
    pub const fn null() -> Self {
        Ref {
            raw: None,
            off: 0,
            typ: PhantomData,
        }
    }
}

impl<T: Data> Ref<T> {
    pub fn load<'g>(&self, guard: &'g Guard) -> Option<&'g T> {
        let whole_ptr = self.raw?.load(guard)?;
        // SAFETY: Since the whole_ptr is loaded, we can deref the sub ptr.
        Some(unsafe { whole_ptr.to_known(self.off as isize) })
    }

    /// If self is non-null within the given transaction, provide a borrowed reference to it.
    pub fn attach<'tx>(self, tx: &'tx Transaction<T::Universe>) -> Option<TxRef<'tx, T>>
    where
        T: Data,
    {
        self.raw?.load_frame(tx.guard())?;
        Some(TxRef {
            detach: self,
            tx: tx.refer(),
            guard: tx.guard(),
        })
    }

    /// SAFETY: adding the given offset to this pointer must produce a valid pointer of type R
    #[inline]
    pub unsafe fn offset<R>(this: Self, offset: u32) -> Ref<R> {
        if offset > i32::MAX as u32 {
            panic!("offset is to large")
        }
        Ref {
            raw: this.raw,
            off: this.off + offset,
            typ: PhantomData,
        }
    }

    pub fn with_nonce(this: Self, nonce: u32) -> NonceRef<T> {
        NonceRef {
            raw: this.raw,
            off: this.off,
            non: nonce,
            typ: PhantomData,
        }
    }

    pub fn store_label(&self, label: &str) {
        if let Some(raw) = &self.raw {
            raw.store_label(label);
        }
    }

    pub fn load_label(&self) -> Option<String> {
        if let Some(raw) = &self.raw {
            raw.load_label(&crossbeam_epoch::pin())
                .map(|s| s.to_string())
        } else {
            None
        }
    }
}

#[macro_export]
macro_rules! static_ref {
    {$vis:vis static $name:ident: Ref<$ty:ty> = $instance:expr;} => {
        static INSTANCE: $crate::framing::DataFraming<$ty> = $crate::framing::DataFraming::new_static($instance);
        static INDIRECTION: $crate::framing::Indirection = $crate::framing::static_indirection(&INSTANCE);

        $vis static $name: $crate::Ref<$ty> = unsafe { $crate::Ref::from_raw(
            $crate::framing::static_objectptr(&INDIRECTION),
            0,
        ) };
    };
    {$vis:vis static $name:ident: Ref<$ty:ty> = $instance:expr;} => {
        $crate::static_ref! {
            $vis static $name: Ref<$ty> = $instance;
        }
    };
    ($ty:ty, $instance:expr) => {
        {
            $crate::static_ref! {
                static REF: Ref<$ty> = $instance;
            }
            REF
        }
    }
}

impl<T> fmt::Debug for Ref<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // TODO: labels
        write!(
            f,
            "Ref<{}>({:?})",
            type_name::<T>(),
            self.raw.unwrap_or(DataPtr::null()).debug_fields()
        )
    }
}

#[test]
fn sizeof_ref() {
    use std::mem::size_of;
    if size_of::<*mut ()>() == 8 {
        assert_eq!(
            std::mem::size_of::<Ref<()>>(),
            8 // pointer
            + 8 // version
            + 4 // offset
            + 4 // padding
        );
    }
}

/// A strong reference to a hornpipe object.
#[repr(C)]
pub struct Own<T: Data> {
    raw: DataPtr,
    // zombie: bool, ?
    typ: PhantomData<Box<T>>,
}

unsafe impl<T: Data> Send for Own<T> {}
unsafe impl<T: Data> Sync for Own<T> {}

impl<T: Data> Own<T> {
    pub fn new_from_box(value: Box<DataFraming<T>>) -> Self {
        let raw = DataFraming::unknown_box(value);
        // SAFETY: all the above conversions are correct
        unsafe { Own::from_raw(DataPtr::new_raw(raw, T::Universe::THIS)) }
    }

    pub fn new(value: T) -> Self {
        Self::new_from_box(DataFraming::new_boxed(value))
    }
}

impl<T: Data> Own<T> {
    #[inline]
    pub(crate) unsafe fn from_raw(raw: DataPtr) -> Self {
        Own {
            raw,
            typ: PhantomData,
        }
    }

    #[inline]
    pub(crate) unsafe fn into_raw(mut this: Self) -> DataPtr {
        this.raw
    }

    #[inline]
    pub fn refer(&self) -> Ref<T> {
        // TODO: consider using `offset_of!(DataFraming<T>, data) as u32` so we avoid an extra add later
        unsafe { Ref::from_raw(self.raw, 0) }
    }

    #[inline]
    pub fn load<'g>(&self, guard: &'g Guard) -> Option<&'g T> {
        let whole_ptr = self.raw.load_frame(guard)?;
        // SAFETY: a successful load guarantees that nothing has changed.
        Some(&unsafe { whole_ptr.to_known_ref() }.data)
    }
}

impl<T: Data> fmt::Debug for Own<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // TODO: labels
        write!(
            f,
            "Own<{}>({:?})",
            type_name::<T>(),
            self.raw.debug_fields(),
        )
    }
}

pub struct OwnOperations;

unsafe impl<T: Data> CopyRef for Own<T> {
    unsafe fn set_partial_ownership(
        _this: &mut crate::share::PartiallyOwned<Self>,
        _matching: u8,
        _status: crate::share::OwnershipStatus,
    ) {
    }

    unsafe fn partial_drop(_this: &mut crate::share::PartiallyOwned<Self>) {}

    fn visit_owned(this: &Self, callback: &mut (impl FnMut(DataPtr) + ?Sized)) {
        callback(this.raw);
    }
}

impl<T: Data> Data for Own<T> {
    type Universe = T::Universe;

    fn finalize<'temp, 'global: 'temp>(
        _internal: &'temp mut crate::share::PartiallyOwned<'temp, Own<T>>,
        _outgoing: &'temp mut crate::share::PartiallyOwned<'global, Own<T>>,
        _ctx: crate::transaction::LifecycleContext<<Own<T> as Data>::Universe>,
    ) {
    }
}

impl<T: Data> Drop for Own<T> {
    fn drop(&mut self) {
        if self.raw.is_null() {
            // Early return
            return;
        }
        // Will drop later at a good time.
        <T as Data>::Universe::sequencer().dropped.push(self.raw);
    }
}

#[test]
fn sizeof_own() {
    use std::mem::size_of;
    if size_of::<*mut ()>() == 8 {
        assert_eq!(
            std::mem::size_of::<Own<()>>(),
            8 // pointer
            + 8 // version
        );
        assert_eq!(
            std::mem::size_of::<Option<Own<()>>>(),
            8 // pointer, null=None
            + 8 // version
        );
    }
}

#[repr(C)]
#[cfg_attr(doc, fundamental)]
pub struct TxRef<'tx, T: Data> {
    pub detach: Ref<T>,
    pub tx: TransactionRef<T::Universe>,
    guard: &'tx Guard,
}

impl<'tx, T: Data> Deref for TxRef<'tx, T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        let Some(raw) = self.detach.raw else {
            // SAFETY: this check must be performed to create a TxRef
            unsafe { unreachable_unchecked() }
        };
        // SAFETY: the version check must be performed with the guard to create a TxRef
        let unknown = unsafe { raw.load_frame_unchecked(self.guard) };
        // SAFETY: a successful load guarantees that nothing has changed.
        unsafe { unknown.data.to_known::<T>(self.detach.off as isize) }
    }
}

impl<'tx, T: Data> TxRef<'tx, T> {
    pub fn guard(this: &Self) -> &'tx Guard {
        this.guard
    }

    pub fn load_tx(this: &Self) -> &'tx TransactionData<T::Universe> {
        this.tx
            .load(this.guard)
            .expect("should never create a TxRef a null transaction")
    }

    pub unsafe fn offset<S: Data<Universe = T::Universe>>(
        this: Self,
        offset: u32,
    ) -> TxRef<'tx, S> {
        TxRef {
            detach: Ref::offset(this.detach, offset),
            tx: this.tx,
            guard: this.guard,
        }
    }
}

impl<'tx, T: Data> Copy for TxRef<'tx, T> {}
impl<'tx, T: Data> Clone for TxRef<'tx, T> {
    fn clone(&self) -> Self {
        *self
    }
}

/// A weak reference to a hornpipe object, subscripted by a nonce.
#[repr(C)]
pub struct NonceRef<T> {
    raw: Option<DataPtr>,
    off: u32,
    pub non: u32,
    typ: PhantomData<Box<T>>,
}

impl<T> Copy for NonceRef<T> {}
impl<T> Clone for NonceRef<T> {
    fn clone(&self) -> Self {
        *self
    }
}

impl<T> NonceRef<T> {
    pub unsafe fn from_raw(raw: DataPtr, off: u32, non: u32) -> Self {
        NonceRef {
            raw: Some(raw),
            off,
            non,
            typ: PhantomData,
        }
    }

    pub unsafe fn as_raw(this: Self) -> (Option<DataPtr>, u32, u32) {
        (this.raw, this.off, this.non)
    }

    #[deprecated]
    pub const fn null() -> Self {
        NonceRef {
            raw: None,
            off: 0,
            non: 0,
            typ: PhantomData,
        }
    }

    pub fn as_ref(self) -> Ref<T> {
        Ref {
            raw: self.raw,
            off: self.off,
            typ: PhantomData,
        }
    }
}

impl<T> fmt::Debug for NonceRef<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // TODO: labels
        write!(
            f,
            "HashRef<{}>({:?}, {:?})",
            type_name::<T>(),
            self.raw.unwrap_or(DataPtr::null()).debug_fields(),
            self.non,
        )
    }
}

#[test]
#[ignore]
fn sizeof_nonceref() {
    use std::mem::size_of;
    if size_of::<*mut ()>() == 8 {
        assert_eq!(
            std::mem::size_of::<NonceRef<()>>(),
            8 // pointer
            + 8 // version
            + 4 // offset
            + 4 // hash
        );
        assert_eq!(
            std::mem::size_of::<Option<NonceRef<()>>>(),
            8 // pointer, null=None
            + 8 // version
            + 4 // offset
            + 4 // hash
        );
    }
}

/// A wrapper for pointer types like [Ref] which implements pointer equality.
#[derive(Clone, Copy, Debug)]
pub struct PtrCmp<P>(pub P);

impl<P> PartialEq for PtrCmp<P>
where
    RefIdentity: for<'a> From<&'a P>,
{
    fn eq(&self, other: &Self) -> bool {
        RefIdentity::from(&self.0).eq(&RefIdentity::from(&other.0))
    }
}

impl<P> Eq for PtrCmp<P> where RefIdentity: for<'a> From<&'a P> {}

impl<P> Ord for PtrCmp<P>
where
    RefIdentity: for<'a> From<&'a P>,
{
    fn cmp(&self, other: &Self) -> cmp::Ordering {
        RefIdentity::from(&self.0).cmp(&RefIdentity::from(&other.0))
    }
}

impl<P> PartialOrd for PtrCmp<P>
where
    RefIdentity: for<'a> From<&'a P>,
{
    fn partial_cmp(&self, other: &Self) -> Option<cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl<P> Hash for PtrCmp<P>
where
    RefIdentity: for<'a> From<&'a P>,
{
    fn hash<H: Hasher>(&self, state: &mut H) {
        RefIdentity::from(&self.0).hash(state)
    }
}

#[repr(C)]
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub struct RefIdentity {
    pub ind: *const Indirection,
    pub gen: u64,
    pub off: u32,
    pub non: u32,
}

unsafe impl Sync for RefIdentity {}
unsafe impl Send for RefIdentity {}

impl From<DataPtr> for RefIdentity {
    fn from(value: DataPtr) -> Self {
        RefIdentity {
            ind: value.indirection_ptr(),
            gen: value.expected_gen(),
            off: 0,
            non: 0,
        }
    }
}

impl<T> From<Ref<T>> for RefIdentity {
    fn from(value: Ref<T>) -> Self {
        RefIdentity {
            ind: match value.raw {
                Some(raw) => raw.indirection_ptr(),
                None => ptr::null(),
            },
            gen: match value.raw {
                Some(raw) => raw.expected_gen(),
                None => 0,
            },
            off: value.off,
            non: 0,
        }
    }
}

impl<T: Data> From<Own<T>> for RefIdentity {
    fn from(value: Own<T>) -> Self {
        RefIdentity {
            ind: value.raw.indirection_ptr(),
            gen: value.raw.expected_gen(),
            off: 0,
            non: 0,
        }
    }
}

impl<T> From<NonceRef<T>> for RefIdentity {
    fn from(value: NonceRef<T>) -> Self {
        RefIdentity {
            ind: match value.raw {
                Some(raw) => raw.indirection_ptr(),
                None => ptr::null(),
            },
            gen: match value.raw {
                Some(raw) => raw.expected_gen(),
                None => 0,
            },
            off: value.off,
            non: value.non,
        }
    }
}

impl From<&'_ DataPtr> for RefIdentity {
    fn from(value: &DataPtr) -> Self {
        RefIdentity {
            ind: value.indirection_ptr(),
            gen: value.expected_gen(),
            off: 0,
            non: 0,
        }
    }
}

impl<T> From<&'_ Ref<T>> for RefIdentity {
    fn from(value: &Ref<T>) -> Self {
        RefIdentity {
            ind: match value.raw {
                Some(raw) => raw.indirection_ptr(),
                None => ptr::null(),
            },
            gen: match value.raw {
                Some(raw) => raw.expected_gen(),
                None => 0,
            },
            off: value.off,
            non: 0,
        }
    }
}

impl<T: Data> From<&'_ Own<T>> for RefIdentity {
    fn from(value: &Own<T>) -> Self {
        RefIdentity {
            ind: value.raw.indirection_ptr(),
            gen: value.raw.expected_gen(),
            off: 0,
            non: 0,
        }
    }
}

impl<T> From<&'_ NonceRef<T>> for RefIdentity {
    fn from(value: &NonceRef<T>) -> Self {
        RefIdentity {
            ind: match value.raw {
                Some(raw) => raw.indirection_ptr(),
                None => ptr::null(),
            },
            gen: match value.raw {
                Some(raw) => raw.expected_gen(),
                None => 0,
            },
            off: value.off,
            non: value.non,
        }
    }
}
