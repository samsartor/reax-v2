\documentclass{article}
\usepackage[margin=1in]{geometry}
\usepackage[utf8]{inputenc}
\usepackage{amsthm}

\title{Notes on Hornpipe Semantics}
\author{Sam Sartor}

\newtheorem{invariant}{Invariant}
\newtheorem{fixup}{Fix-up}

\begin{document}

\maketitle

This document formally describes the behavior of the Hornpipe state management framework. It would be exhausting to specify the specific exact behavior in every situation the framework might encounter. Instead these notes provide rules for what behaviors \emph{can} and \emph{cannot} be observed \emph{given an after-the-fact viewpoint on the entire program execution}.

Just as a conceptual ``variable'' is stored in a number of low-level slots, conceptual ``reads and writes'' are implemented by receiving and sending messages in those slots. Each message is associated with the transaction which created it, and each transaction has two associated timestamps:
\begin{itemize}
    \item The viewpoint - identifies the version of the application state which the transaction should read from.
    \item The visibility - identifies the version of the application state created when the transaction committed.
\end{itemize}

These do not have to be actual timestamps, in the sense that they store the wall-clock time when a version was made. In fact, they can be any sort of totally-ordered identifier. In the current implementation this is a simple integer that increments every time a transaction begins or is committed.

When we refer to the ``order of transactions'' we are referring to the ordering created by the viewpoint timestamp. The ``order of messages'' refers to the ordering produced firstly by the transaction viewpoints associated with those messages, and secondly by the actual order of creation \emph{within a single transaction}. Any observable behaviors occurring within the scope of a transaction with viewpoint $v$ are said to ``use viewpoint $v$''.

Generally the semantics below are largely concerned with constraining which messages should have ``observable effects'' and which should be ``ignored''. Although these terms can be used somewhat loosely, generally a message has an ``observable effect'' if changing its contents would change the data provided to user code. If within the scope of a transaction with viewpoint $v$ the content of a message must not have any effect on user code, we say it is ``ignored by $v$''.

\section{System Invariants}

The 3 rules below, taken together, ensure that Hornpipe displays serializable consistency.  Invariant \ref{visibility-ignoring} defines how writes can interact with reads. Invariants \ref{visibility-ordering} and \ref{all-or-none} can be thought of as stating ``transaction writes occur atomically, fully before or fully after any reads or writes''.

If all these invariants were perfectly maintained, messages would not actually need separate viewpoints and visibilities. Invariant \ref{visibility-ordering} requires the visibilities of some messages to establish the same ordering as the viewpoints which sent those messages. In practice it will be useful to maintain the distinction.

\begin{invariant}
Any message using viewpoint $v$ and visibility $s < v$ can only be ignored if it would have no observable effect.
\label{visibility-ignoring}
\end{invariant}

\begin{invariant}
For any viewpoints $v_A < v_B$ and for any message using visibility $s$ and viewpoint $v_A$, it must be that $v_A < s < v_B$.
\label{visibility-ordering}
\end{invariant}

\begin{invariant}
If message using viewpoint $v$ has any observable effect, all effects of messages using $v$ must also be observable at the same time.
\label{all-or-none}
\end{invariant}

During actual execution, it may be the case that \emph{not all messages exist} or that \emph{not all messages have assigned visibility}. This is because messages are generated dynamically, and only made visible on transaction commit.

A viewpoint $v_B$ must naturally ignore these ``invisible'' messages. If all ignored invisible messages are given late visibility so that $s > v_B$, we can violate invariant \ref{visibility-ordering} whenever the message was sent from viewpoint $v_A < v_B$. If ignored invisible messages are given early visibility so that $s < v_B$, we can violate invariant \ref{visibility-ignoring}. We choose to violate invariant \ref{visibility-ordering} by allowing the following execution steps, where steps 1 and 2 can be concurrent:
\begin{enumerate}
    \item An invisible, effectful message is sent from viewpoint $v_A$
    \item Viewpoint $v_B > v_A$ ignores the message
    \item The message is given visibility $s > v_B$
\end{enumerate}

In most cases late visibility assignment does not create inconsistency because ignored messages would not have an effect (e.g. writes to a variable do not effect reads of unrelated variables). But in cases where inconsistency might occur, it needs to be resolved in a predictable fashion.

\section{Fix-ups}

We can identify conflicts and the messages which caused them by storing information about when and where the messages are ignored. When an message has visibility assigned, we determine for each if our message was ignored when it should not have been, and handle the conflict if so. Higher granularity in our logging means fewer false positives triggered on messages which would not actually have an effect. For example, a viewpoint could identify the specific variable and visibility range in which messages were ignored, or only the variable as a whole. Then for serializability to be preserved, there are 2 different ``fix-ups'' available at time of detection:

\begin{fixup}
    Prevent the ignored message from having visibility assigned.
    \label{fixup-drop-message}
\end{fixup}

\begin{fixup}
    Prevent the ignoring viewpoint from having any effect.
    \label{fixup-drop-tx}
\end{fixup}

In order to preserve invariant \ref{all-or-none}, it must be the case that fix-up \ref{fixup-drop-message} drops any other messages created by the associated viewpoint. In this sense, both fix-up \ref{fixup-drop-message} and fix-up \ref{fixup-drop-tx} work by aborting a transaction, and only differ in whether it is the ignoring or the ignored transaction that aborts.

At this point our approach is very similar to serializable snapshot isolation (SSI) by Cahill et al. It is simplified by the availability of a predetermined total order on transactions (viewpoint). Relaxing viewpoint to a partial ordering would reduce the number of conflicts, but require our method to detect triples of unserializable transactions, not only pairs of ignoring/ignored transactions.

\section{CRDTs}

Often the end user does not require strict adherence to invariant \ref{visibility-ordering}, in which case other fix-ups are possible. This is most common when the system is used to implement a conflict-free replicated data type (CRDT), as messages on such data types are always commutative. If a CRDT is operation-based (the implementation consists only of the message list itself), we can use a trivial fix-up:

\begin{fixup}
    Assign visibility to the ignored message as normal.
    \label{fixup-crdt-operation}
\end{fixup}

If the CRDT is state-based, using a merge function to combine states from different replicas, then we need a slightly more complex fix-up:

\begin{fixup}
    Merge the state from the viewpoint which applied the message with the state from the viewpoint which ignored the message, using the given function.
    \label{fixup-crdt-state}
\end{fixup}

Semantically speaking, the entire system is one unified state, making boundaries between CRDT states rather fuzzy. As such, it is most common to use a delta-state-based CRDT with an update function to apply messages to state:

\begin{fixup}
    Update the state from the viewpoint which ignored the message using the the message which was ignored and using the given function.
    \label{fixup-crdt-delta-state}
\end{fixup}

Note that the effects of a fix-up must only be observed once the effects of the ignoring transaction and the ignored message are also observed. To properly retain consistency, the reverse should also be true: the effects of the ignoring transaction and ignored message can not both be observed without observing the effects of all resulting fix-ups. Howover, this may not always be feasible.

In a single-player system, it is reasonable to block visibility changes and viewpoint assignments while fix-ups are generated and applied. In this way, the system will never be observed in an inconsistent state. But this is unreasonable in a distributed system where all players must reach consensus on a visibility change and on resulting fix-ups before any can proceed. Allowing players to display writes without confirmation usually results in inconsistency, since fix-ups discovered later can not apply retroactively. However, certain combinations of fix-ups may allow players to arrive at the same observed state without inconsistency. For example, when the only fix-ups are on CRDTs.

\section{Dataflow}

The major advantage of our system is reactivity: the management of application dataflow from upstream state to downstream state, and the guarantee that all downstream state appears to be instantly updated. This is only achievable in the transactional environment described above because the dataflow system is a state-based CRDT.

Specifically, we introduce ``computed variables'', which produce messages based on the application of a function to messages produced by arbitrary upstream variables. For the system to be considered consistent, the following must hold:
\begin{invariant}
    When the latest messages of a computed variable are observed, they must exactly match those produced by application of the compute function to the latest observable upstream messages.
    \label{data-flow-observability}
\end{invariant}

At first glance this seems like an operation-based CRDT, since it consists only of the system's existing message list. But dataflow messages are not commutative: to uphold invariant \ref{data-flow-observability} all computed messages must have visibility no later than the latest upstream message, and to uphold \ref{visibility-ordering} those messages must be seen in a specific order. Instead the CRDT has a state consisting of all messages visible from a given viewpoint, and uses the following merge function:

\begin{center}
    Traverse the downstream$\rightarrow$upstream graph of all variables in postorder, applying computation functions and assigning visibility to resulting messages.
\end{center}

This is not the formulation we use in practice, since it would be absurdly expensive to compute state merges. Instead we use a dataflow system that propagates dirtied flags instantly and then only applies computation functions as needed. However, the above ``eager dataflow'' is useful in concept, since the actual ``lazy dataflow'' mimics it to the extent possible.

\section{Lazy Dataflow}

The lazy dataflow is justified by noticing invariant \ref{data-flow-observability} only applies ``when the latest messages of a computed variable are observed''. It is not necessary for computed messages and upstream messages to gain visibility at the same moment of execution. Instead they can become visible just-in-time, when their being ignored would have an observable effect.

To achieve this we introduce two message types which only exist as part of the CRDT state, and are not otherwise observable to end users: \texttt{Dirty(var)}, \texttt{NextDownstream(var, var)}, and \texttt{NextUpstream(var, var[])}.

If the latest visible message on a variable is \texttt{Dirty}, the user can determine that there are invisible computed messages on that variable, and begin the postorder downstream$\rightarrow$upstream graph traversal using the latest visible \texttt{NextUpstream} messages. When a computation runs, it establishes a new list of all upstream variables and stores it using a new \texttt{NextUpstream} message. The computation also creates a new \texttt{NextDownstream} message for each upstream variable, so that it is possible to send \texttt{Dirty} variables in the first place.

When an message is sent outside of the postorder traversal, a preorder traversal of the upstream$\rightarrow$downstream graph is done using the \texttt{NextDownstream} messages, with \texttt{Dirty} messages created for every visited variable.

To maintain invariant \ref{data-flow-observability} we use a bespoke implementation of fix-up \ref{fixup-crdt-delta-state}. As soon as an ignored \texttt{Dirty} message is given visibility and the ignoring transaction commits (in any order), the preorder traversal is performed starting at the dirtied variable. The same occurs as soon as an ignored \texttt{NextDownstream} message is given visibility and the ignoring transaction commits. It is not strictly necissary to fix-up ignored \texttt{NextUpstream} messages as that would also require some \texttt{NextDownstream} messages to be ignored, and \texttt{Dirty} messages created by those other fix-ups are sufficient to ensure the postorder traversal will rerun.

\emph{Proof of commutativity is left as an exercise for future me.}

\end{document}
