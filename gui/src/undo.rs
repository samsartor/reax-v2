use hornpipe_core::{append_undo_ref, logic, AttachedObjectRef, TransactionRef};
use hornpipe_interface::prelude::*;
use hornpipe_interface::{deque::Deque, global::OtherTransaction, stream::Stream};

hornpipe_extern! {
    pub struct UndoRedoStack {
        tx_stream: Own<Stream<Own<OtherTransaction>>>,
        undo_stack: Own<Deque<Own<OtherTransaction>>>,
        redo_stack: Own<Deque<Own<OtherTransaction>>>,
    }
}

impl UndoRedoStack {
    pub fn empty() -> Own<Self> {
        Self::new(Stream::new(), Deque::new(), Deque::new())
    }
}

impl<'tx> UndoRedoStack<AttachedObjectRef<'tx>> {
    pub fn undo(&self) -> bool {
        let Some(to_undo) = self.undo_stack().attach(Self::transaction(self)).pop_back() else {
            return false;
        };
        append_undo_ref(Self::transaction(self).refer(), to_undo.detached.to_core());
        true
    }
}
