use druid::widget::{Controller, ControllerHost};
use druid::{Command, Event, EventCtx, ExtEventSink, LifeCycle, Selector, Widget, WidgetId};
use hornpipe_core::{create_now, logic, ObjectOwn, SlotElement, Transaction};
use std::any::type_name;
use std::sync::Arc;
use std::task::Wake;

pub trait WidgetExt<T>: Widget<T> + Sized {
    fn on_react(
        self,
        function: impl Fn(&Transaction, &mut Self, &mut EventCtx) + Send + 'static,
    ) -> ControllerHost<Self, OnReact<Self>> {
        ControllerHost::new(self, OnReact::new(function))
    }
}

impl<W, T> WidgetExt<T> for W where W: Widget<T> {}

struct ReactorWake;

const REACTOR_WAKE_OP: Selector<ReactorWake> = Selector::new("hornpipe_reactor_wake");

struct HornpipeWaker {
    sink: ExtEventSink,
    id: WidgetId,
}

impl Wake for HornpipeWaker {
    fn wake(self: std::sync::Arc<Self>) {
        let _ = self
            .sink
            .submit_command(REACTOR_WAKE_OP, ReactorWake, self.id);
    }

    fn wake_by_ref(self: &std::sync::Arc<Self>) {
        let _ = self
            .sink
            .submit_command(REACTOR_WAKE_OP, ReactorWake, self.id);
    }
}

pub struct OnReact<W> {
    label: &'static str,
    function: Box<dyn Fn(&Transaction, &mut W, &mut EventCtx) + Send>,
    waker: Option<ObjectOwn>,
}

impl<W> OnReact<W> {
    pub fn new<F>(function: F) -> Self
    where
        F: Fn(&Transaction, &mut W, &mut EventCtx) + Send + 'static,
    {
        OnReact {
            label: type_name::<F>(),
            function: Box::new(function),
            waker: None,
        }
    }
}

impl<T, W> Controller<T, W> for OnReact<W>
where
    W: Widget<T>,
{
    fn event(
        &mut self,
        child: &mut W,
        ctx: &mut EventCtx,
        event: &Event,
        data: &mut T,
        env: &druid::Env,
    ) {
        match event {
            Event::Command(com) if com.is(REACTOR_WAKE_OP) => {
                let tx = Transaction::start();
                tx.refer()
                    .store_label(format_args!("druid update for {}", self.label));
                let Some(waker) = &self.waker else { return };
                let c = logic::computation(&tx, waker.slot_id(0));
                (self.function)(&tx, child, ctx);
                drop(c);
                tx.commit();
            }
            event => child.event(ctx, event, data, env),
        }
    }

    fn lifecycle(
        &mut self,
        child: &mut W,
        ctx: &mut druid::LifeCycleCtx,
        event: &LifeCycle,
        data: &T,
        env: &druid::Env,
    ) {
        match event {
            LifeCycle::WidgetAdded => {
                let waker = Arc::new(HornpipeWaker {
                    sink: ctx.get_external_handle(),
                    id: ctx.widget_id(),
                });
                let waker = create_now(&[], [SlotElement::NULL, SlotElement::Waker(waker.into())]);
                self.waker = Some(waker);
                ctx.submit_command(Command::new(REACTOR_WAKE_OP, ReactorWake, ctx.widget_id()));
            }
            _ => (),
        }
        child.lifecycle(ctx, event, data, env)
    }

    fn update(
        &mut self,
        child: &mut W,
        ctx: &mut druid::UpdateCtx,
        old_data: &T,
        data: &T,
        env: &druid::Env,
    ) {
        child.update(ctx, old_data, data, env)
    }
}
