use hornpipe_core::{create_now, logic, ObjectOwn, SlotElement, Transaction};
use std::any::type_name;
use std::marker::PhantomData;
use std::panic::{catch_unwind, resume_unwind, AssertUnwindSafe};
use xilem::view::{self, View, ViewMarker};
use xilem::widget::ChangeFlags;
use xilem::{Id, MessageResult};

pub struct Reactor<V> {
    label: &'static str,
    // TODO: this box is not _really_ needed, just helps
    // make some nasty trait stuf less nasty
    function: Box<dyn Fn(&Transaction) -> V + Send>,
    view: PhantomData<V>,
}

impl<V> Reactor<V> {
    pub fn new<F>(function: F) -> Self
    where
        F: Fn(&Transaction) -> V + Send + 'static,
    {
        Reactor {
            label: type_name::<F>(),
            view: PhantomData,
            function: Box::new(function),
        }
    }
}

pub struct ReactorState<V, S> {
    inner_id: Id,
    inner_view: V,
    inner_state: S,
    inner_dirty: bool,
    waker: ObjectOwn,
}

impl<V> ViewMarker for Reactor<V> {}

impl<AppState, Action, V> View<AppState, Action> for Reactor<V>
where
    V: View<AppState, Action>,
{
    type State = ReactorState<V, V::State>;

    type Element = V::Element;

    fn build(&self, cx: &mut view::Cx) -> (Id, Self::State, Self::Element) {
        let this_id = Id::next();
        cx.with_id(this_id, |cx| {
            let waker = create_now(&[], [SlotElement::NULL, SlotElement::Waker(cx.waker())]);
            let tx = Transaction::start();
            tx.refer()
                .store_label(format_args!("xilem build for {}", self.label));
            let c = logic::computation(&tx, waker.slot_id(0));
            let inner_view = (self.function)(&tx);
            drop(c);
            tx.commit();
            let (inner_id, inner_state, elem) = inner_view.build(cx);
            let state = ReactorState {
                inner_id,
                inner_view,
                inner_state,
                inner_dirty: false,
                waker,
            };
            (this_id, state, elem);
        })
    }

    fn rebuild(
        &self,
        cx: &mut view::Cx,
        _prev: &Self,
        id: &mut Id,
        state: &mut Self::State,
        element: &mut Self::Element,
    ) -> ChangeFlags {
        cx.with_id(*id, |cx| {
            // TODO: do we need to put a new cx.waker() in state.waker?
            let tx = Transaction::start();
            tx.refer()
                .store_label(format_args!("xilem rebuild for {}", self.label));
            let this_dirty = logic::recompute_slot(tx.refer(), tx.guard(), state.waker.slot_id(0));
            match (this_dirty, state.inner_dirty) {
                (true, _) => {
                    let c = logic::computation(&tx, state.waker.slot_id(0));
                    let view = (self.function)(&tx);
                    drop(c);
                    tx.commit();
                    let out = view.rebuild(
                        cx,
                        &state.inner_view,
                        &mut state.inner_id,
                        &mut state.inner_state,
                        element,
                    );
                    state.inner_view = view;
                    out
                }
                (false, true) => {
                    tx.commit();
                    state.inner_dirty = false;
                    state.inner_view.rebuild(
                        cx,
                        &state.inner_view,
                        &mut state.inner_id,
                        &mut state.inner_state,
                        element,
                    )
                }
                (false, false) => ChangeFlags::empty(),
            }
        })
    }

    fn message(
        &self,
        id_path: &[Id],
        state: &mut Self::State,
        message: Box<dyn std::any::Any>,
        app_state: &mut AppState,
    ) -> MessageResult<Action> {
        if id_path.is_empty() {
            // Only possible message is an AsyncWake triggered by hornpipe dirty
            MessageResult::RequestRebuild
        } else if id_path.starts_with(&[state.inner_id]) {
            let res =
                state
                    .inner_view
                    .message(&id_path[1..], &mut state.inner_state, message, app_state);
            if let MessageResult::RequestRebuild = res {
                state.inner_dirty = true;
            }
            res
        } else {
            MessageResult::Stale(message)
        }
    }
}
