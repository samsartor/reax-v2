use dioxus::prelude::*;
use hornpipe_gui::dioxus::use_hornpipe;
use hornpipe_interface::hornpipe_extern;

fn main() {
    dioxus_desktop::launch(app);
}

hornpipe_extern! {
    struct App {
        mut count: i32,
    }
}

fn app(cx: Scope) -> Element {
    let root = use_state(&cx, || App::new(0));

    use_hornpipe(&cx, |tx| {
        let count = root.attach(&tx).count();
        cx.render(rsx! {
            label { count.get().to_string() }
            button {
                onclick: move |_| count.detached.operate(|c| c.set(c.get() + 1)),
                "Count Up",
            }
            button {
                onclick: move |_| count.detached.operate(|c| c.set(c.get() - 1)),
                "Count Down",
            }
        })
    })
}
