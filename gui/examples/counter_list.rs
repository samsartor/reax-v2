use hornpipe_interface::prelude::*;
use once_cell::sync::Lazy;

hornpipe_extern! {
    struct Counter {
        mut count: i32,
    }

    struct App {
        mut counters: Own<Collection<u64, Own<Counter>>>,
    }
}

static ROOT: Lazy<Own<App>> = Lazy::new(|| App::new(Collection::new()));

fn main() {
    use std::env::args;

    match args().nth(1).as_deref() {
        #[cfg(feature = "dioxus")]
        Some("dioxus") => {
            dioxus_desktop::launch(crate::dioxus::app);
        }
        #[cfg(feature = "xilem")]
        Some("xilem") => {
            let app = ::xilem::App::new((), crate::xilem::app);
            ::xilem::AppLauncher::new(app).run()
        }
        #[cfg(feature = "druid")]
        Some("druid") => {
            let app = ::druid::WindowDesc::new(crate::druid::app_widget());
            ::druid::AppLauncher::with_window(app).launch(()).unwrap();
        }
        Some(backend) => panic!("not compiled with {backend:?}"),
        None => panic!("pass a backend"),
    }
}

#[cfg(feature = "xilem")]
mod xilem {
    use hornpipe_gui::xilem::Reactor;
    use hornpipe_interface::prelude::*;
    use xilem::view::{button, h_stack, list, v_stack, View};

    pub fn counter(
        counters: Ref<Collection<u64, Own<super::Counter>>>,
        index: usize,
    ) -> impl View<()> {
        Reactor::new(move |tx| {
            let elem = counters.attach(tx).unwrap().find(&(index as u64)).unwrap();
            let count = elem.count();
            h_stack((
                button(format!("{} + 1", count.get()), move |_| {
                    count.detached.operate(|c| c.set(c.get() + 1));
                }),
                button(format!("{} - 1", count.get()), move |_| {
                    count.detached.operate(|c| c.set(c.get() - 1));
                }),
            ))
        })
    }

    pub fn app(_: &mut ()) -> impl View<()> {
        Reactor::new(|tx| {
            let counters = super::ROOT.attach(tx).counters().get();
            v_stack((
                list(counters.len(), move |index| {
                    counter(counters.detached, index)
                }),
                button("Add Counter", move |_| {
                    counters.detached.operate(|c| {
                        let len = c.len();
                        c.insert(&(len as u64), super::Counter::new(0));
                    });
                }),
            ))
        })
    }
}

#[cfg(feature = "druid")]
mod druid {
    use druid::widget::{Button, Flex, Label, Widget};
    use hornpipe_gui::druid::WidgetExt as _;
    use hornpipe_interface::prelude::*;

    pub fn counter_widget(count: Slot<i32>) -> impl Widget<()> {
        Flex::row()
            .with_child(Label::new("").on_react(move |tx, label, ctx| {
                label.set_text(count.attach(tx).unwrap().get().to_string());
                ctx.request_update();
            }))
            .with_child(Button::new("Count Up").on_click(move |_, _, _| {
                count.operate(|c| c.set(c.get() + 1));
            }))
            .with_child(Button::new("Count Down").on_click(move |_, _, _| {
                count.operate(|c| c.set(c.get() - 1));
            }))
    }

    pub fn app_widget() -> impl Widget<()> {
        let counters = super::ROOT.operate(|root| root.counters().get().detached);
        Flex::column().on_react(move |tx, old_flex, ctx| {
            let counters = counters.attach(tx).unwrap();
            let mut flex = Flex::column();
            for counter in counters.iter() {
                flex.add_child(counter_widget(counter.count().detached));
            }
            flex.add_child(Button::new("Add Counter").on_click(move |_, _, _| {
                counters.detached.operate(|c| {
                    let len = c.len();
                    c.insert(&(len as u64), super::Counter::new(0));
                });
            }));
            *old_flex = flex;
            ctx.children_changed();
        })
    }
}

#[cfg(feature = "dioxus")]
mod dioxus {
    use dioxus::prelude::*;
    use hornpipe_gui::dioxus::use_hornpipe;
    use hornpipe_interface::prelude::*;

    #[inline_props]
    #[allow(non_snake_case)]
    pub fn Counter(cx: Scope, counter: Ref<super::Counter>) -> Element {
        use_hornpipe(&cx, |tx| {
            let count = counter.attach(&tx)?.count();
            cx.render(rsx! {
                label { count.get().to_string() }
                button {
                    onclick: move |_| count.detached.operate(|c| c.set(c.get() + 1)),
                    "Count Up",
                }
                button {
                    onclick: move |_| count.detached.operate(|c| c.set(c.get() - 1)),
                    "Count Down",
                }
                br {}
            })
        })
    }

    pub fn app(cx: Scope) -> Element {
        use_hornpipe(&cx, |tx| {
            let counters = super::ROOT.attach(&tx).counters().get();
            cx.render(rsx! {
                counters.iter().map(|c| rsx! { Counter { counter: c.detached } })
                button {
                    onclick: move |_| counters.detached.operate(|c| {
                        let len = c.len();
                        c.insert(&(len as u64), super::Counter::new(0));
                    }),
                    "Add Counter"
                }
            })
        })
    }
}
